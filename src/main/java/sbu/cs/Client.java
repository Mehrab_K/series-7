package sbu.cs;

import java.io.*;

public class Client
{
    public static void main(String[] args) throws IOException
    {
        String filePath = args[0];

        MyClient client = new MyClient();
        client.connect("localhost");
        client.sendFile(filePath);

        client.close();
    }
}
