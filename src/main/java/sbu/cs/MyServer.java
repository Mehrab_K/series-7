package sbu.cs;

import java.io.*;
import java.net.*;

public class MyServer
{
    private static final int portNumber = 8080;
    private ServerSocket server;
    private Socket socket;
    private DataInputStream reader;
    private String fileName;
    private byte[] data;

    public MyServer() throws IOException
    {
        server = new ServerSocket(portNumber);
    }

    public void waitForConnection() throws IOException
    {
        socket = server.accept();
        reader = new DataInputStream(socket.getInputStream());
    }

    public void read() throws IOException
    {
        fileName = reader.readUTF();
        int len = reader.readInt();
        data = new byte[len];

        reader.readFully(data, 0, len);
    }

    public void createFile(String directory) throws IOException
    {
        FileOutputStream file = new FileOutputStream(directory + "/" + fileName);
        file.write(data, 0, data.length);
    }

    public void close() throws IOException
    {
        server.close();
        socket.close();
        reader.close();
    }
}
