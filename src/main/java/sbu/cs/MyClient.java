package sbu.cs;

import java.io.*;
import java.net.Socket;
import java.nio.file.*;

public class MyClient
{
    private static final int portNumber = 8080;
    private Socket socket;
    private DataOutputStream writer;

    public void connect(String hostAddress) throws IOException
    {
        socket = new Socket(hostAddress, portNumber);
        writer = new DataOutputStream(socket.getOutputStream());
    }

    /**
     * sending fileName and byteArray size and byteArray to server
     * @param filePath
     * @throws IOException
     */
    public void sendFile(String filePath) throws IOException
    {
        byte[] bytes = Files.readAllBytes(Paths.get(filePath));
        writer.writeUTF(new File(filePath).getName());
        writer.writeInt(bytes.length);
        writer.write(bytes);
    }

    public void close() throws IOException
    {
        socket.close();
        writer.close();
    }
}
