package sbu.cs;

import java.io.*;

public class Server
{
    public static void main(String[] args) throws IOException
    {
        String directory = args[0];

        MyServer server = new MyServer();
        server.waitForConnection();
        server.read();
        server.createFile(directory);

        server.close();
    }
}
